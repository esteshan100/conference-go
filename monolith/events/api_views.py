from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Conference, Location, State
from common.json import ModelEncoder
from .acls import get_picture, get_cords, get_weather
import json


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
    ]

    def get_extra_data(self, o):
        # pass the city value
        picture_data = get_picture(o.city)
        # set the url of the picture
        picture_url = picture_data["picture"]  # Extract the picture URL
        return {"state": o.state.abbreviation, "picture_url": picture_url}

class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
    ]


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder,
    }

    def get_extra_data(self, o):
        city = o.location.city
        location_coords = get_cords(city)
        weather_data = get_weather(location_coords)
        return {"weather": weather_data}


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
        )
    else:
        # Get the body of the JSON and turn it into a dict
        content = json.loads(request.body)
        # Error handling
        try:
            # Get the location object by the ID found in content["location"]
            location = Location.objects.get(id=content["location"])
            # Replace the ID in content["location"] with the corresponding location object
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "INVALID LOCATION ID"},
                status=400,
            )
    conference = Conference.objects.create(**content)
    return JsonResponse(
        conference,
        encoder=ConferenceDetailEncoder,
        safe=False,
    )


def api_show_conference(request, id):
    conference = Conference.objects.get(id=id)
    return JsonResponse(
        conference,
        encoder=ConferenceDetailEncoder,
        safe=False
    )


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationListEncoder,
        )
    else:
        content = json.loads(request.body)
    try:
        state = State.objects.get(abbreviation=content["state"])
        content["state"] = state
    except State.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid state abbreviation"},
            status=400,
        )
    location = Location.objects.create(**content)
    return JsonResponse(
        location,
        encoder=LocationDetailEncoder,
        safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):
    if request.method == "GET":
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        # Delete the specific id
        count, throw_away_data = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        # Turn the data into a dictionary
        content = json.loads(request.body)
        # error handeling
        try:
            if "state" in content:
                    # Retrieve the State object whose abbreviation matches the value in content["state"]
                    state = State.objects.get(abbreviation=content["state"])
                    # Replace the abbreviation in content["state"] with the corresponding State object
                    content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "INVALID STATE ABBREVIATION"},
                status=400,
            )
        # Content updated by id
        Location.objects.filter(id=id).update(**content)
        # Get object by id
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False
        )
