import json
import requests
# Importing API keys from a separate keys file
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

# Function to get a picture related to the city from Pexels
def get_picture(city):
    payload = {"query": city}  # Parameters for the search
    headers = {"Authorization": PEXELS_API_KEY}  # Authorization header with API key
    # Request to Pexels API
    response = requests.get("https://api.pexels.com/v1/search", headers=headers, params=payload)
    content = json.loads(response.content)  # Parsing the JSON response
    picture = {
        "picture": content["photos"][0]["src"]["original"]  # Extracting the URL of the original picture
    }
    return picture

# Function to get coordinates of a given city from OpenWeatherMap
def get_cords(city):
    payload = {"q": city, "appid": OPEN_WEATHER_API_KEY}  # Parameters for the search
    # Request to OpenWeatherMap API
    response = requests.get("http://api.openweathermap.org/geo/1.0/direct", params=payload)
    content = json.loads(response.content)  # Parsing the JSON response
    location = {
        "latitude": content[0]["lat"],  # Extracting latitude
        "longitude": content[0]["lon"],  # Extracting longitude
        "appid": OPEN_WEATHER_API_KEY
    }
    return location

# Function to get weather data for a given location from OpenWeatherMap
def get_weather(location):
    payload = {
        "lat": location["latitude"],  # Latitude of location
        "lon": location["longitude"],  # Longitude of location
        "units": "imperial",  # Units for temperature
        "appid": location["appid"]  # API key
    }
    # Request to OpenWeatherMap weather API
    response = requests.get("https://api.openweathermap.org/data/2.5/weather", params=payload)
    content = json.loads(response.content)  # Parsing the JSON response
    weather = {
        "temp": content["main"]["temp"],  # Extracting temperature
        "description": content["weather"][0]["description"]  # Extracting weather description
    }
    return weather

# Code to test the above functions
if __name__ == "__main__":
    city_test = "Miami"  # Test city
    picture = get_picture(city_test)
    print(picture)  # Print picture details

    location = get_cords(city_test)
    print(location)  # Print location details

    weather = get_weather(location)
    print(weather)  # Print weather details
